# The Task

Create a simple React application that shows a list of construction companies, each with the
following information:

- Company name
- Logo (you may use a placeholder image, e.g. using https://placekitten.com/)
- Specialties (e.g. Excavation, Plumbing, Electrical)
- City

The following should be possible:
- Search for a company by typing into a search field. The search term gets matched only
  against the company name and the list of companies is filtered based on the search term
  in real time as the user is typing.
- Filter the list using a set of checkboxes to include only those companies which offer a
  particular speciality (e.g. only Plumbing).


  Create a simple API based on Node.js that returns the list of companies to the frontend. The
  API can read the data from a simple JSON source, no database setup is required.
  In addition, make a list of ideas on how you would improve this application if you had more time
  to work on it.
  
# Installation

1. Clone project.
2. `yarn install` in `/api` folder.
3. `yarn start` in `/api` folder.
4. `yarn install` in `/app` folder.
5. `yarn start` in `/app` folder.
6. Open [this link](http://localhost:3000) in your browser.

# Implementation Details
The API is a very simple Express application, nothing fancy.

The frontend app utilizes the CRA React setup, so it's pretty basic too.

To save time and make things look better I used the MaterialUI library for user interface components.

To make things a bit spicy I made a little experiment and created a custom implementation of a global state store based on Flux and Observer patterns. It seems to work pretty well and I hope you'll enjoy it.


# Possible improvements

According to the KISS principle, any optimization should be done only when it is necessary. I believe this implementation of the given task is good enough and does not need any. Though improvements may be considered after receiving new objectives, for example:

- If the company's data becomes a subject of changes, maybe we need to add a database layer?
- If the backend logic becomes more complicated, maybe we need to introduce MVC or similar architecture? Or even utilize one of the suitable frameworks, e.g. NestJS, FeathersJS, Sails?
- If a team of more than one engineer will work on the project, maybe we need to add linting?
- If a data flow will become more complicated, maybe we need to use Redux with all its superpowers?
- If the amount of information is on a Big Data scale, we need lots of optimizations on both backend and frontend.
- If we need more types of data to show, maybe we need an application router?
- If we need user-specific restrictions, maybe we need to add authentication and authorization layers?
- Do we need server rendering?
- Testing suit?
- Better error handling and monitoring?

The options of optimization are countless, but they should depend on given product requirements.
