const express = require('express');
const path = require('path');
const cors = require('cors');

const app = express();
const port = 3001;

app.use(cors());

app.get('/', (req, res) => {
  res.send({ 'Available methods': ['GET /data'] });
});

app.get('/data', (req, res) => {
  res.sendFile(path.resolve('../data.json'));
});

app.listen(port, () => {
  console.log(`Listening for requests at http://localhost:${port}`);
});
