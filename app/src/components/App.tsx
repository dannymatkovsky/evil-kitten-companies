import React from 'react';
import Layout from './Layout';
import { CompaniesPage } from '../pages/Companies.page';

function App() {
  return (
    <Layout>
      <CompaniesPage />
    </Layout>
  );
}

export default App;
