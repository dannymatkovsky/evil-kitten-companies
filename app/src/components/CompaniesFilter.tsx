import React from 'react';
import {
  Box,
  Checkbox,
  FormControlLabel,
  FormGroup,
  TextField,
  FormLabel,
} from '@mui/material';
import { setNameFilter, setSpecialitiesFilter } from '../store/actions';
import { useStore } from '../hooks/useStore.hook';

export function CompaniesFilter() {
  const { specialities, specialitiesFilter } = useStore();

  const nameFilterChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
    setNameFilter(e.target.value);
  };

  const specialitiesFilterChanged = (
    e: React.ChangeEvent<HTMLInputElement & { checked: boolean }>
  ) => {
    const value = parseInt(e.target.value);
    setSpecialitiesFilter(
      e.target.checked
        ? [...specialitiesFilter, value]
        : specialitiesFilter.filter((id) => id !== value)
    );
  };

  return (
    <Box
      component="form"
      sx={{
        display: 'flex',
        padding: 5,
        '& > :not(:first-of-type)': { marginLeft: 5, width: '25ch' },
      }}
      noValidate
      autoComplete="off"
    >
      <TextField
        label="Name filter"
        variant="outlined"
        onChange={nameFilterChanged}
      />
      <FormGroup>
        <FormLabel component="legend">Speciality filter</FormLabel>
        {specialities.map((speciality) => (
          <FormControlLabel
            control={
              <Checkbox
                value={speciality.id}
                onChange={specialitiesFilterChanged}
              />
            }
            label={speciality.name}
            key={speciality.id}
          />
        ))}
      </FormGroup>
    </Box>
  );
}
