import React from 'react';
import { Box, Grid, Typography } from '@mui/material';
import { useFilteredCompanies } from '../hooks/useFilteredCompanies';
import { Company } from '../interfaces/company.interface';
import { CompanyCard } from './CompanyCard';

export function CompaniesList() {
  const companies: Company[] = useFilteredCompanies();

  return (
    <Box sx={{ padding: 5 }}>
      {companies.length === 0 ? (
        <Typography component="div">No Evil Kitten Companies found</Typography>
      ) : (
        <Grid container spacing={2}>
          {companies.map((company) => (
            <Grid item md={4} lg={3} key={company.id}>
              <CompanyCard company={company} />
            </Grid>
          ))}
        </Grid>
      )}
    </Box>
  );
}
