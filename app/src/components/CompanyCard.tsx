import React from 'react';
import { Company } from '../interfaces/company.interface';
import { Box, Card, CardContent, CardMedia, Typography } from '@mui/material';
import { useStore } from '../hooks/useStore.hook';

export function CompanyCard({ company }: { company: Company }) {
  const { specialities } = useStore();

  // Could be memoized
  const specialitiesMap: Record<number, string> = specialities.reduce(
    (obj, { id, name }) => ({ ...obj, [id]: name }),
    {}
  );

  return (
    <Card sx={{ display: 'flex' }}>
      <CardMedia
        component="img"
        sx={{ width: 150 }}
        image={company.logo}
        alt={company.name}
      />

      <Box sx={{ display: 'flex', flexDirection: 'column' }}>
        <CardContent sx={{ flex: '1 0 auto' }}>
          <Typography component="div" variant="h5">
            {company.name}
          </Typography>
          <Typography variant="subtitle1" component="div">
            {company.city}
          </Typography>
          <Typography variant="body1" component="div">
            {company.specialtyIds.map((id) => specialitiesMap[id]).join(', ')}
          </Typography>
        </CardContent>
      </Box>
    </Card>
  );
}
