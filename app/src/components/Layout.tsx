import React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { ReactJSXElement } from '@emotion/react/types/jsx-namespace';

function Layout({
  children,
}: {
  children: ReactJSXElement | ReactJSXElement[];
}) {
  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h5" component="h1">
            Evil Kitten Companies
          </Typography>
        </Toolbar>
      </AppBar>

      {children}
    </>
  );
}

export default Layout;
