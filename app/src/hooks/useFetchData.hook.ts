import { useEffect } from 'react';
import {
  setCompanies,
  setDataFetched,
  setSpecialities,
} from '../store/actions';
import { Company } from '../interfaces/company.interface';
import { Speciality } from '../interfaces/speciality.interface';

export function useFetchData(): void {
  useEffect(() => {
    fetch('http://localhost:3001/data', {
      method: 'get',
    })
      .then((response) => response.json())
      .then(
        ({
          companies,
          specialities,
        }: {
          companies: Company[];
          specialities: Speciality[];
        }) => {
          setCompanies(companies);
          setSpecialities(specialities);
          setDataFetched(true);
        }
      )
      .catch((error) => {
        alert(`Error: ${error.message}`);
      });
  }, []);
}
