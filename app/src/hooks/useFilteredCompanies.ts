import { useStore } from './useStore.hook';
import { Company } from '../interfaces/company.interface';

export function useFilteredCompanies(): Company[] {
  const { companies, nameFilter, specialitiesFilter } = useStore();

  return companies
    .filter(
      (company: Company) =>
        !nameFilter ||
        company.name.toLowerCase().includes(nameFilter.toLowerCase())
    )
    .filter(
      (company: Company) =>
        specialitiesFilter.length === 0 ||
        company.specialtyIds.filter((id) => specialitiesFilter.includes(id))
          .length > 0
    );
}
