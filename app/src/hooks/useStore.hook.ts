import { useEffect, useState } from 'react';
import { getState, subscribe, unsubscribe } from '../store/store';
import { State } from '../interfaces/state.interface';
import { StoreObserver } from '../interfaces/storeObserver.interface';

export function useStore() {
  const [state, setState] = useState<State>(getState());
  const observer: StoreObserver = (currentState: State) => {
    setState(currentState);
  };

  useEffect(() => {
    subscribe(observer);
    return () => unsubscribe(observer);
  }, []);

  return state;
}
