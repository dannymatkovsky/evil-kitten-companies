export interface Company {
  id: number;
  name: string;
  logo: string;
  specialtyIds: number[];
  city: string;
}
