import { Company } from './company.interface';
import { Speciality } from './speciality.interface';

export interface State {
  companies: Company[];
  specialities: Speciality[];
  dataFetched: boolean;
  nameFilter: string;
  specialitiesFilter: number[];
}
