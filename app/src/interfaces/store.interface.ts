import { State } from './state.interface';
import { StoreObserver } from './storeObserver.interface';

export interface Store {
  state: State;
  observers: StoreObserver[];
}
