import { State } from './state.interface';

export type StoreObserver = (state: State) => void;
