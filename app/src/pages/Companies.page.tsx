import React from 'react';
import { useStore } from '../hooks/useStore.hook';
import { useFetchData } from '../hooks/useFetchData.hook';
import { Loader } from '../components/Loader';
import { CompaniesList } from '../components/CompaniesList';
import { CompaniesFilter } from '../components/CompaniesFilter';

export function CompaniesPage() {
  const { dataFetched } = useStore();
  useFetchData();

  return !dataFetched ? (
    <Loader />
  ) : (
    <>
      <CompaniesFilter />
      <CompaniesList />
    </>
  );
}
