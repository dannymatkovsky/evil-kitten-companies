import { Company } from '../interfaces/company.interface';
import { dispatch } from './dispatcher';
import {
  SET_COMPANIES,
  SET_DATA_FETCHED,
  SET_NAME_FILTER,
  SET_SPECIALITIES,
  SET_SPECIALITIES_FILTER,
} from './actionTypes';
import { Speciality } from '../interfaces/speciality.interface';

export function setCompanies(companies: Company[]): void {
  dispatch({ type: SET_COMPANIES, companies });
}

export function setSpecialities(specialities: Speciality[]): void {
  dispatch({ type: SET_SPECIALITIES, specialities });
}

export function setDataFetched(dataFetched: boolean): void {
  dispatch({ type: SET_DATA_FETCHED, dataFetched });
}

export function setNameFilter(nameFilter: string): void {
  dispatch({ type: SET_NAME_FILTER, nameFilter });
}

export function setSpecialitiesFilter(specialitiesFilter: number[]): void {
  dispatch({ type: SET_SPECIALITIES_FILTER, specialitiesFilter });
}
