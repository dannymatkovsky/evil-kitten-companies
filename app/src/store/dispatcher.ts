import {
  SET_COMPANIES,
  SET_DATA_FETCHED,
  SET_NAME_FILTER,
  SET_SPECIALITIES,
  SET_SPECIALITIES_FILTER,
} from './actionTypes';
import { getStateUpdater } from './store';
import { State } from '../interfaces/state.interface';

export function dispatch({
  type,
  ...payload
}: { type: string } & Record<string, any>) {
  const updateState = getStateUpdater();
  switch (type) {
    case SET_COMPANIES:
      updateState(
        (state: State): State => ({
          ...state,
          companies: payload.companies,
        })
      );
      break;
    case SET_SPECIALITIES:
      updateState(
        (state: State): State => ({
          ...state,
          specialities: payload.specialities,
        })
      );
      break;
    case SET_DATA_FETCHED:
      updateState(
        (state: State): State => ({
          ...state,
          dataFetched: payload.dataFetched,
        })
      );
      break;
    case SET_NAME_FILTER:
      updateState(
        (state: State): State => ({
          ...state,
          nameFilter: payload.nameFilter,
        })
      );
      break;
    case SET_SPECIALITIES_FILTER:
      updateState(
        (state: State): State => ({
          ...state,
          specialitiesFilter: payload.specialitiesFilter,
        })
      );
      break;
  }
}
