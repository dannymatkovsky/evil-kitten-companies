import { State } from '../interfaces/state.interface';

export const initialState: State = {
  companies: [],
  specialities: [],
  dataFetched: false,
  nameFilter: '',
  specialitiesFilter: [],
};
