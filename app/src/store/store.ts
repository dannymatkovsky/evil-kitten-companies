import { StoreObserver } from '../interfaces/storeObserver.interface';
import { State } from '../interfaces/state.interface';
import { Store } from '../interfaces/store.interface';
import { initialState } from './initialState';

const store: Store = {
  state: initialState,
  observers: [],
};

export function subscribe(observer: StoreObserver): void {
  store.observers.push(observer);
}

export function unsubscribe(observer: StoreObserver): void {
  store.observers = store.observers.filter(
    (item: StoreObserver) => item !== observer
  );
}

function notify(): void {
  store.observers.forEach((observer: StoreObserver): void =>
    observer(store.state)
  );
}

export function getStateUpdater() {
  return (callback: (state: State) => State): void => {
    store.state = callback(store.state);
    notify();
  };
}

export function getState(): State {
  return store.state;
}
